import { async, inject, TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { PostService, Post } from './post.service';

describe('PostService', () => {
  let postService: PostService;
  let httpMock: HttpTestingController;

  // beforeEach(() => {
  //   TestBed.configureTestingModule({});
  //   service = TestBed.inject(PostService);
  // });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PostService],
    });
    postService = TestBed.get(PostService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should fetch posts as an Observable', async(
    inject(
      [HttpTestingController, PostService],
      (httpMock: HttpTestingController, postService: PostService) => {
        const postItem = [
          {
            userId: 1,
            id: 1,
            title:
              'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
            body:
              'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto',
          },
          {
            userId: 1,
            id: 2,
            title: 'qui est esse',
            body:
              'est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla',
          },
          {
            userId: 1,
            id: 3,
            title:
              'ea molestias quasi exercitationem repellat qui ipsa sit aut',
            body:
              'et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut',
          },
        ];

        postService.getPosts().subscribe((posts: Post[]) => {
          expect(posts.length).toBe(3); // Test for 3 posts
          expect(typeof posts[0].userId).toBe('number'); // Test if id is a number
        });

        let req = httpMock.expectOne(
          'https://jsonplaceholder.typicode.com/posts'
        );
        expect(req.request.method).toBe('GET'); // Test if request method is GET

        req.flush(postItem);
        httpMock.verify();
      }
    )
  ));
});
